import java.io.*;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.util.Arrays;

public class BeastAttack
{

    public static void main(String[] args) throws Exception
    {
		byte[] ciphertext=new byte[1024]; // will be plenty big enough

		// e.g. this prints out the result of an encryption with no prefix

		System.out.println("Full ciphertext with no prefix:");
		int length=callEncrypt(null, 0, ciphertext);	    

		for(int i=0; i<length; i++)
		{
			System.out.print(String.format("%02x ", ciphertext[i]));
		}
		System.out.println("\n");	


		// TASK 1-------------------
		System.out.println("Total message size = " + length);
		System.out.println("Plaintext size = " + (length - 8));
		// --------------------------

		// TASK 2-------------------
		int numBlocks = (length - 1) / 8;

		byte[] retrievedFullMessage = new byte[numBlocks * 8];
		
		// FIRST BLOCK
		
		// start building up a new message for this block
		byte[] retrievedBlockMessage = new byte[8];
		for (int byteNumber = 0; byteNumber < 8; ++byteNumber) {

			System.out.println("WORKING ON BYTE #" + (byteNumber + 1));

			// first force the 0s byte prefix onto the plaintext
			byte[] zeroPrefix = new byte[7-byteNumber];
			Arrays.fill(zeroPrefix, (byte)0);
			callEncrypt(zeroPrefix, zeroPrefix.length, ciphertext);
	
			// get the ciphertext block, note block 0 is the the IV
			byte[] cBlock = extractBlock(1, ciphertext);
			// Get the IV
			byte[] IV0 = extractBlock(0, ciphertext);
	
			// check space
			if (testByte((int) ' ', byteNumber, 1, ciphertext, cBlock, IV0, retrievedFullMessage)) {
				continue;
			}
	
			
			// check lowercase first			
			for (int b = 122; b >= 45; --b) {
				if (testByte(b, byteNumber, 1, ciphertext, cBlock, IV0, retrievedFullMessage)) {
					break;
				}
			}
		}
		
		// SECOND BLOCK		
		
		for (int blockNumber = 2; blockNumber <= numBlocks; blockNumber++) {		
			for (int byteNumber = 0; byteNumber < 8; ++byteNumber) {
				System.out.println("WORKING ON BYTE #" + (byteNumber + (blockNumber - 1 )* 8));
		    
				//first force the 0s byte prefix onto the plaintext
				byte[] zeroPrefix = new byte[7-byteNumber];
				Arrays.fill(zeroPrefix, (byte)0);
				callEncrypt(zeroPrefix, zeroPrefix.length, ciphertext);
		
				// get the ciphertext block, note block 0 is the the IV
				byte[] cBlock = extractBlock(blockNumber, ciphertext);
				// Get the IV
				byte[] previousCBlock = extractBlock(blockNumber - 1, ciphertext);
				
				if (testByte2((int) ' ', byteNumber, blockNumber, ciphertext, cBlock, previousCBlock, retrievedFullMessage)) {
					continue;
				}
				
				// check lowercase first			
				for (int b = 122; b >= 45; --b) {
					if (testByte2(b, byteNumber, blockNumber, ciphertext, cBlock, previousCBlock, retrievedFullMessage)) {
						break;
					}
				}
			}
		} 
		
	} // main
	
	/*
	testByte - the byte to check
	byteNumber - the byte number we are working on
	blockNumber - the block number we are working on
	ciphertext - the actual ciphertext we are decrypting
	cBlock - the ciphertext block we are working on
	IV0 - the block prior to the ciphertext block
	retrievedMessage - what we have got so far
	*/
	static boolean testByte(int testByte, int byteNumber, int blockNumber, byte[] ciphertext, byte[] cBlock,  byte[] IV0, byte[] retrievedMessage) throws Exception {

		byte x = (byte)testByte;
		System.out.println((char) x);
		boolean correctIV = false;
		
		byte[] predictedIV;
		byte[] actualIV;
		
			// make host encrypt the prefix (0; 0; 0; 0; 0; 0; 0; x)
		while (!correctIV) {
			predictedIV = nextIV(extractIV(ciphertext));
				
			byte[] prefix = new byte[8];
			//for (int iPrefix = 0; iPrefix < 8; ++iPrefix) {
			int numZeros = 7 - byteNumber;
				

			for (int j = 0; j < numZeros; ++j) {
				prefix[j] = (byte)(0 ^ predictedIV[j] ^ IV0[j]);						
			}
					
			// fill the rest with the message we have decrypted so far
			for (int k = numZeros; k < 7; ++k) {
				prefix[k] = (byte)(predictedIV[k] ^ IV0[k] ^ retrievedMessage[k - numZeros]);
			}
					
			// Now we set the last byte to the guess, x
			prefix[7] = (byte)(predictedIV[7] ^ IV0[7] ^ x);

			callEncrypt(prefix, prefix.length, ciphertext);
			
			actualIV = extractIV(ciphertext);
			
			if (Arrays.equals(predictedIV, actualIV)) {
				// The IV we used was correct
				correctIV = true;					
			}			
		}		
		
		// look only at the second cipher block, and see which one matches (c1;...; c8).
		byte[] newCBlock = extractBlock(1, ciphertext);
			
		if (Arrays.equals(cBlock, newCBlock)) {
			// Hooray!
			System.out.println("Retrieved " + x);
			retrievedMessage[byteNumber] = x;
			printAscii(retrievedMessage);
			return true;
		}			
		
		return false; // this is not the byte you are looking for.

	}
		
	/*
	testByte - the byte to check
	byteNumber - the byte number we are working on
	blockNumber - the block number we are working on
	ciphertext - the actual ciphertext we are decrypting
	cBlock - the ciphertext block we are working on
	IV0 - the block prior to the ciphertext block
	retrievedMessage - what we have got so far
	*/
	static boolean testByte2(int testByte, int byteNumber, int blockNumber, byte[] ciphertext, byte[] cBlock,  byte[] IV0, byte[] retrievedMessage) throws Exception {
		// For each byte, x	
		byte x = (byte)testByte;
		System.out.println((char) x);
		boolean correctIV = false;
		
		byte[] predictedIV;
		byte[] actualIV;
				
		// make host encrypt the prefix (0; 0; 0; 0; 0; 0; 0; x)
		while (!correctIV) {
			predictedIV = nextIV(extractIV(ciphertext));				
			
			byte[] prefix = new byte[8];
			int numZeros = 7 - byteNumber;					
	
			for (int j = 0; j < 7; ++j) {
				prefix[j] = (byte)(retrievedMessage[j + byteNumber + 1 + (blockNumber - 2) * 8] ^ predictedIV[j] ^ IV0[j]);						
			}
			// Now we set the last byte to the guess, x
			prefix[7] = (byte)(predictedIV[7] ^ IV0[7] ^ x);

			callEncrypt(prefix, prefix.length, ciphertext);
			
			actualIV = extractIV(ciphertext);
			
			if (Arrays.equals(predictedIV, actualIV)) {
				// The IV we used was correct
				correctIV = true;
			}			
		}		
		
		// look only at the second cipher block, and see which one matches (c1;...; c8).
		byte[] newCBlock = extractBlock(1, ciphertext);
			
		if (Arrays.equals(cBlock, newCBlock)) {
			// Hooray!
			System.out.println("Retrieved " + x);
			retrievedMessage[(blockNumber - 1) * 8 + byteNumber] = x;
			printAscii(retrievedMessage);
			return true;
		}
		return false; // this is not the byte you are looking for.
	}	

	// Task 1 method - print the encrypted IV multiple times to spot a pattern
	// Prints first 8 bytes and perfrom n encryptions
	static void printIVAndEncrypt(int n, byte[] ciphertext) throws Exception {

		System.out.println("First 8 bytes - IV:") ;
			for (int i = 0; i < n; ++i)
			{
				callEncrypt(null, 0, ciphertext);	    

				for(int j=0; j<8; j++)
				{
					System.out.print(String.format("%02x ", ciphertext[j]));
				}
				
				System.out.println();
				byte[] nextPredictedIV = nextIV(extractIV(ciphertext));
				for(int j=0; j<8; j++)
				{
					System.out.print(String.format("%02x ", nextPredictedIV[j]));
				}
		
				System.out.println();
				for(int j=0; j<8; j++)
				{
					System.out.print(Integer.parseInt(String.format("%02x", ciphertext[j]), 16) + " ");
				}

				System.out.println("\n");
			}
	}
	
	static void printIV(byte[] arr){
		for(int j=0; j<8; j++){
			System.out.print(String.format("%02x ", arr[j]));
		}
		System.out.println();
	}
	
	static void printArr(byte[] arr) {
		for(int j=0; j< arr.length; j++){
			System.out.print(String.format("%02x ", arr[j]));
		}
		System.out.println();
	}
	
	// Converts the array from int to ASCII
	static void printAscii(byte[] arr) {
		for(int j=0; j<arr.length; j++){
			System.out.print((char) arr[j]);
		}
		System.out.println();
	}
	
	// Attempts to predict the next IV, given the previous IV
	// This attempt takes a guess that the lost byte of the IV, will be the previous value plus 10
	static byte[] nextIV (byte[] currentIV) {
		byte[] nextIV = currentIV;
		nextIV[7] = (byte)(currentIV[7] + 10);
		return nextIV; 
	}
	
	// returns a byte array of first 8 bytes i.e. IV
	static byte[] extractIV (byte[] arr) {
		return extractBlock(0, arr);
	}
	
	// returns 8 byte block from array, n is the block number, starting from 0 for the IV
	static byte[] extractBlock (int n, byte[] arr) {
		return Arrays.copyOfRange(arr, n * 8, (n+1)*8);
	}
	 


    // a helper method to call the external programme "encrypt" in the current directory
    // the parameters are the plaintext, length of plaintext, and ciphertext; returns length of ciphertext
    static int callEncrypt(byte[] prefix, int prefix_len, byte[] ciphertext) throws IOException
    {
	HexBinaryAdapter adapter = new HexBinaryAdapter();
	Process process;
	
	// run the external process (don't bother to catch exceptions)
	if(prefix != null)
	{
	    // turn prefix byte array into hex string
	    byte[] p=Arrays.copyOfRange(prefix, 0, prefix_len);
	    String PString=adapter.marshal(p);
	    process = Runtime.getRuntime().exec("./encrypt "+PString);
	}
	else
	{
	    process = Runtime.getRuntime().exec("./encrypt");
	}

	// process the resulting hex string
	String CString = (new BufferedReader(new InputStreamReader(process.getInputStream()))).readLine();
	byte[] c=adapter.unmarshal(CString);
	System.arraycopy(c, 0, ciphertext, 0, c.length); 
	return(c.length);
    }
}
