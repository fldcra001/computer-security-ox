import java.util.Random;
import java.io.*;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;



class DESPrac
{
    public static void main(String[] args) throws IOException
    {
        long testP=0x1234567887654321L;
        long testK=0x33333333333333L;
        
		System.out.println("TASK 1:\n");
		long result = TwoRoundModifiedDES(testK, testP);
		System.out.println(Long.toHexString(result));
		
		timeEncryptions();
		
		System.out.println("\nTASK 2:\n");
		System.out.println("Each S-box accepts a 6 bit input and outputs a 4 bit value. These substitutions are deterministic, and the output values are uniformally distributed. Since we have 2^6 possible inputs mapping to 2^4 possible outputs, each output occurs occurs for 4 different inputs. Given a uniformally distributed input, we know that each output value must occur for four inputs, thus they are uniformally distributed.");
		
		System.out.println("\nS Table 1:\n");
		differentialDistributionTable(1);
		
		System.out.println("\nTASK 3:\n");
		findK(0x0080800260000000L, 0, 0xD);
		
		System.out.println("\nTASK 4:\n");
		/* 
		
		We know that deltaR1 = deltaL0 ^ deltaF1 and that deltaR1 gets used as input to the second round feistel cipher.
		If we trace this through the cipher we have:
		
				deltaP = 0x 40 00 40 10 02 00 00 00
		
				deltaR0 = 0x 02 00 00 00
				deltaE1 = 0x 00 04 00 00 00 00 00 00 (each char is 6 bits)
				deltaS1a = 0x 00 ; deltaS1b = 0x 04 ; delta S1c = 0x 00 ...
				
				Using STable 2: deltaO1b = 0x7 and deltaO1(notb) = 0x0 with probability 12/64
				deltaF1 = 0x 40 00 40 10 with probability 12/64
				deltaR1 = deltaF1 XOR delta L0 = 0x 00 00 00 00 with probability 12/64
				deltaL1 = deltaR0 = 0x 02 00 00 00
				
				So now with probability 12 / 64:
				
				deltaL2 = deltaR1 = 0x 00 00 00 00
				
				Since deltaR1 = 0 which is the input to the second feistel round, the output must be 0 too.
				Therefore deltaF2 = 0
				deltaR2 = deltaF2 XOR deltaL1 = 0x 02 00 00 00
		*/
		
		findK(0x4000401002000000L, 1, 0x7);
		
    }
    
    // find the possible key values for a given plaintext differential for the key part specified by keyPartNumber
    // outputToLookFor is the relevant STables output differeintial that we are looking for.
    static void findK(long deltaP, int keyPartNumber, int outputToLookFor) {
    	
    	Random prng = new Random();
    	HashSet<Byte> possibleK = new HashSet<Byte>();    	
    	HashSet<Integer> PI = new HashSet<Integer>();
    	
    	// The STable applicable to the key part we are working on
    	byte[] STable = STables[keyPartNumber]; 
    	
    	final long R2_CHARACTERISTIC = deltaP & MASK32;
    	
    	//System.out.println(Long.toHexString(R2_CHARACTERISTIC));
    	
    	// insert values 0..63 into the hashset, we will then remove impossible values
    	for (byte b = 0; b < 64; ++b)
    		possibleK.add(b);	 
    		   	
    	int tryCount = 0;
    		
    	// Now keep generating random plaintexts until we hit the characteristic
    	while (true) {
    	    ++tryCount;
    		PI = new HashSet<Integer>();
    		
    		long P = prng.nextLong();
    		long P_ = P ^ deltaP;
    		
    		// S is 48 bits and we need to isolate the 6 bits we are interested in
    		final  int S_SHIFT_AMOUNT = 42 - (6 * keyPartNumber);
    		
    		// _ ***************
    		long L0_ = (P_>>32)&MASK32; //first 32
        	long R0_ = P_&MASK32; // last 32
        	long E1_ = EBox(R0_);
       	 	long S1_ = E1_; // using (1) - input xor       	      	 	
       	 	
       	 	long O1_ = SBox(S1_);       	 	

       	 	int Sa_ = (int)((S1_ >> S_SHIFT_AMOUNT) & 63);	
       	    int Oa_ = (int)STable[Sa_];
       	    
       	 	long P1_ = PBox(O1_);
       	 	
        	long F1_ = P1_; // using (2)
        	
        	long R1_ = F1_ ^ L0_;	
        	long L1_ = R0_;
        	
        	long L2_ = R1_;
        	        	
        	//*******************
        	
        	// ***************
    		long L0 = (P>>32)&MASK32; //first 32
        	long R0 = P&MASK32; // last 32
        	long E1 = EBox(R0);
       	 	long S1 = E1; // using (1)
       	      	 
       	    int Sa=(int)((S1 >> S_SHIFT_AMOUNT) & 63);	
       	    int Oa = (int)STable[Sa];
       	 	
       	 	long O1 = SBox(S1);

       	 	long P1 = PBox(O1);
       	 	
        	long F1 = P1; // using (2)
        	
        	long R1 = F1 ^ L0;	
        	long L1 = R0;
        	
        	long L2 = R1;
        	        	
        	//*******************
        	// Input to SBox
			long inputXOR = Sa_ ^ Sa;
        	        	
        	long C_ = 0;
        	long C = 0;
        	
        	try {
        		C_ = callEncrypt(P_);
        		C = callEncrypt(P);
        	} catch (IOException e) {
        		System.out.println(e);
        		System.exit(0);
        	}
        	
        	// Calculate ciphertext differentials
        	long deltaC = C ^ C_;
        	long deltaL2C = (deltaC>>32)&MASK32;
        	long deltaR2C = deltaC&MASK32;
        	long deltaF2 = deltaR2C ^ L1 ^ L1_;
        	long deltaL1 = L1^L1_;
        	
        	// check if we have hit the characteristic
        	if (deltaL2C == 0x00000000 && deltaR2C == R2_CHARACTERISTIC) {
        		// HIT
        		
        		// Compute all values from inputXOR -> outputToLookFor
    			for (int s1 = 0; s1 < 64; ++s1) {
    				for (int s2 = 0; s2 < 64; ++s2) {   					

						byte o1 = STable[s1]; 
						byte o2 = STable[s2];

						// Check if we have found two possible inputs that go to the correct output
						if (((s1 ^ s2) == inputXOR) && ((o1 ^ o2) == outputToLookFor)){
							// FOUND							
							// Add possible keys to PI
							PI.add(s1 ^ Sa); PI.add(s2 ^ Sa_);
						}
					}
				}
								
				// Now remove impossible keys
				for (int k = 0; k < 64; ++k) {
					if (!PI.contains(k))
						possibleK.remove((byte)k);
				}
				
				System.out.println("Current set of possible keys for key part " + (keyPartNumber + 1) + ":");
    			System.out.println("\t" + possibleK);

				if (possibleK.size() == 2 || tryCount > 100)
        			break;
        	}
        	// MISS              	
    	}
    	
    	System.out.println("Final set of possible keys for key part " + (keyPartNumber + 1) + ":");
    	System.out.println("\t" + possibleK);    	
    }

	// Measures how long it takes to perform 1000 encryptions.
	// Also outputs how long a brute force attack on the key would take.
	static void timeEncryptions() {
	
		long testP=0x1234567887654321L;
        long testK=0x33333333333333L;
        
		long startTime = System.nanoTime();   
		
		for (int i = 0; i < 1000; ++i) { 
			long result = TwoRoundModifiedDES(testK++, testP++);
		}

		long estimatedTime = System.nanoTime() - startTime;
		long estimatedTimeOneEncrypt = estimatedTime / 1000;
		
		System.out.println("Time taken to perform 1000 encryptions = " + estimatedTime + " nano seconds");
		System.out.println("Time taken to perform 1 encryptions = " + estimatedTimeOneEncrypt + " nano seconds");
		System.out.println("On average we expect to try 2^55 keys in an exhaustion attack.");
		System.out.println("Thus total time = time for 1 encryption * 2^55 = ");
		
		BigInteger numCalls = new BigDecimal(Math.pow(2, 55)).toBigInteger();
		BigInteger totalTimeNS =  BigInteger.valueOf(estimatedTimeOneEncrypt).multiply(numCalls);
		
		BigInteger yearFactor  = new BigInteger("31540000000000000");
		System.out.println("\t" + totalTimeNS.divide(yearFactor) + " years");
	}

	// Prints out the differential distribution table for the specified S Box.
    static void differentialDistributionTable (int SBoxNumber)
    {
		int[][] output = new int[64][16];
    	
        for (int s = 0; s < 64; ++s) {
			for (int s_ = 0; s_ < 64; ++s_) {
				int deltaS = s ^ s_;
				byte o = STables[SBoxNumber-1][s];
				byte o_ = STables[SBoxNumber-1][s_];				
				int deltaO = (int) (o ^ o_);				
				
				++output[deltaS][deltaO];				
			}
		}
		
		for (int i = 0; i < 64; ++i) {
			System.out.println(Arrays.toString(output[i]));
		}		
    }

    // constants for &-ing with, to mask off everything but the bottom 32- or 48-bits of a long
    static long MASK32 = 0xffffffffL;
    static long MASK48 = 0xffffffffffffL;

    static long TwoRoundModifiedDES(long K, long P) // input is a 56-bit key "long" and a 64-bit plaintext "long", returns the ciphertext
    {
        long L0=(P>>32)&MASK32; // watch out for the sign extension!
        long R0=P&MASK32;
        long K1=K&MASK48;
        long K2=(K>>8)&MASK48;

        long L1=R0;
        long R1=L0^Feistel(R0, K1);

        long L2=R1;
        long R2=L1^Feistel(R1, K2);

        long C=L2<<32 | R2;

        return(C);
    }

    static long Feistel(long R, long K) // input is a 32-bit integer and 48-bit key, both stored in 64-bit signed "long"s; returns the output of the Feistel round
    {
        long F;
        
        long E = EBox(R);
        long S = E ^ K;
        long O = SBox(S);
        long P = PBox(O);
        long K32 = K & MASK32; // last 32 bits
        
        F = P ^ K32;

    	return(F);
    }

    // NB: these differ from the tables in the DES standard because the latter are encoded in a strange order

    static final byte[] S1Table={
     3,  7,  5,  1, 12,  8,  2, 11, 10,  3, 15,  6,  7, 12,  8,  2,
    13,  0, 11,  4,  6,  5,  1, 14,  0, 10,  4, 13,  9, 15, 14,  9,
     4,  1,  2, 12, 11, 14, 15,  5, 14,  7,  8,  3,  1,  8,  5,  6,
     9, 15, 12, 10,  0, 11, 10,  0, 13,  4,  7,  9,  6,  2,  3, 11,
    };

    static final byte[] S2Table={
    13,  1,  2, 15,  8, 13,  4,  8,  6, 10, 15,  3, 11,  7,  1,  4,
    10, 12,  9,  5,  3,  6, 14, 11,  5,  0,  0, 14, 12,  9,  7,  2,
     7,  2, 11,  1,  4, 14,  1,  7,  9,  4, 12, 10, 14,  8,  2, 13,
     0, 15,  6, 12, 10,  9, 13,  0, 15,  3,  3,  5,  5,  6,  8, 11,
    };

    static final byte[] S3Table={
    14,  0,  4, 15, 13,  7,  1,  4,  2, 14, 15,  2, 11, 13,  8,  1,
     3, 10, 10,  6,  6, 12, 12, 11,  5,  9,  9,  5,  0,  3,  7,  8,
     4, 15,  1, 12, 14,  8,  8,  2, 13,  4,  6,  9,  2,  1, 11,  7,
    15,  5, 12, 11,  9,  3,  7, 14,  3, 10, 10,  0,  5,  6,  0, 13,
    };

    static final byte[] S4Table={
    10, 13,  0,  7,  9,  0, 14,  9,  6,  3,  3,  4, 15,  6,  5, 10,
     1,  2, 13,  8, 12,  5,  7, 14, 11, 12,  4, 11,  2, 15,  8,  1,
    13,  1,  6, 10,  4, 13,  9,  0,  8,  6, 15,  9,  3,  8,  0,  7,
    11,  4,  1, 15,  2, 14, 12,  3,  5, 11, 10,  5, 14,  2,  7, 12,
    };

    static final byte[] S5Table={
     7, 13, 13,  8, 14, 11,  3,  5,  0,  6,  6, 15,  9,  0, 10,  3,
     1,  4,  2,  7,  8,  2,  5, 12, 11,  1, 12, 10,  4, 14, 15,  9,
    10,  3,  6, 15,  9,  0,  0,  6, 12, 10, 11,  1,  7, 13, 13,  8,
    15,  9,  1,  4,  3,  5, 14, 11,  5, 12,  2,  7,  8,  2,  4, 14,
    };

    static final byte[] S6Table={
     2, 14, 12, 11,  4,  2,  1, 12,  7,  4, 10,  7, 11, 13,  6,  1,
     8,  5,  5,  0,  3, 15, 15, 10, 13,  3,  0,  9, 14,  8,  9,  6,
     4, 11,  2,  8,  1, 12, 11,  7, 10,  1, 13, 14,  7,  2,  8, 13,
    15,  6,  9, 15, 12,  0,  5,  9,  6, 10,  3,  4,  0,  5, 14,  3,
    };

    static final byte[] S7Table={
    12, 10,  1, 15, 10,  4, 15,  2,  9,  7,  2, 12,  6,  9,  8,  5,
     0,  6, 13,  1,  3, 13,  4, 14, 14,  0,  7, 11,  5,  3, 11,  8,
     9,  4, 14,  3, 15,  2,  5, 12,  2,  9,  8,  5, 12, 15,  3, 10,
     7, 11,  0, 14,  4,  1, 10,  7,  1,  6, 13,  0, 11,  8,  6, 13,
    };

    static final byte[] S8Table={
     4, 13, 11,  0,  2, 11, 14,  7, 15,  4,  0,  9,  8,  1, 13, 10,
     3, 14, 12,  3,  9,  5,  7, 12,  5,  2, 10, 15,  6,  8,  1,  6,
     1,  6,  4, 11, 11, 13, 13,  8, 12,  1,  3,  4,  7, 10, 14,  7,
    10,  9, 15,  5,  6,  0,  8, 15,  0, 14,  5,  2,  9,  3,  2, 12,
    };

    // STables[i-1][s] is the output for input s to S-box i
    static final byte[][] STables={S1Table, S2Table, S3Table, S4Table, S5Table, S6Table, S7Table, S8Table};

    static long SBox(long S) // input is a 48-bit integer stored in 64-bit signed "long"
    {
        // Split I into eight 6-bit chunks
        int Sa=(int)((S>>42));
        int Sb=(int)((S>>36)&63);
        int Sc=(int)((S>>30)&63);
        int Sd=(int)((S>>24)&63);
        int Se=(int)((S>>18)&63);
        int Sf=(int)((S>>12)&63);
        int Sg=(int)((S>>6)&63);
        int Sh=(int)(S&63);
        // Apply the S-boxes
        byte Oa=S1Table[Sa];
        byte Ob=S2Table[Sb];
        byte Oc=S3Table[Sc];
        byte Od=S4Table[Sd];
        byte Oe=S5Table[Se];
        byte Of=S6Table[Sf];
        byte Og=S7Table[Sg];
        byte Oh=S8Table[Sh];
        // Combine answers into 32-bit output stored in 64-bit signed "long"
        long O=(long)Oa<<28 | (long)Ob<<24 | (long)Oc<<20 | (long)Od<<16 | (long)Oe<<12 | (long)Of<<8 | (long)Og<<4 | (long)Oh;
        return(O);
    }

    static long EBox(long R) // input is a 32-bit integer stored in 64-bit signed "long"
    {
        // compute each 6-bit component
        long Ea=(R>>27)&31 | (R&1)<<5;
        long Eb=(R>>23)&63;
        long Ec=(R>>19)&63;
        long Ed=(R>>15)&63;
        long Ee=(R>>11)&63;
        long Ef=(R>>7)&63;
        long Eg=(R>>3)&63;
        long Eh=(R>>31)&1 | (R&31)<<1;
        // 48-bit output stored in 64-bit signed "long"
        long E=(long)Ea<<42 | (long)Eb<<36 | (long)Ec<<30 | (long)Ed<<24 | (long)Ee<<18 | (long)Ef<<12 | (long)Eg<<6 | (long)Eh;
        return(E);
    }

    static final int[] Pbits={
    16,  7, 20, 21,
    29, 12, 28, 17,
     1, 15, 23, 26,
     5, 18, 31, 10,
     2,  8, 24, 14,
    32, 27,  3,  9,
    19, 13, 30,  6,
    22, 11,  4, 25
    };

    // this would have been a lot faster as fixed binary operations rather than a loop
    static long PBox(long O) // input is a 32-bit integer stored in 64-bit signed "long"
    {
        long P=0L;
        for(int i=0; i<32; i++)
        {
            P|=((O>>(32-Pbits[i]))&1) << (31-i);
        }
        return(P);
    }

    // a helper method to call the external programme "desencrypt" in the current directory
    // the parameter is the 64-bit plaintext to encrypt, returns the ciphertext
    static long callEncrypt(long P) throws IOException
    {

        Process process = Runtime.getRuntime().exec("./desencrypt "+Long.toHexString(P));
        String CString = (new BufferedReader(new InputStreamReader(process.getInputStream()))).readLine();

        // we have to go via BigInteger otherwise the signed longs cause incorrect parsing
        long C=new BigInteger(CString, 16).longValue();

        return(C);
    }

}

/*

****************
	OUTPUT
****************

TASK 1:

c844e31b90953751
Time taken to perform 1000 encryptions = 1288948 nano seconds
Time taken to perform 1 encryptions = 1288 nano seconds
On average we expect to try 2^55 keys in an exhaustion attack.
Thus total time = time for 1 encryption * 2^55 = 
	1471 years

TASK 2:

Each S-box accepts a 6 bit input and outputs a 4 bit value. These substitutions are deterministic, and the output values are uniformally distributed. Since we have 2^6 possible inputs mapping to 2^4 possible outputs, each output occurs occurs for 4 different inputs. Given a uniformally distributed input, we know that each output value must occur for four inputs, thus they are uniformally distributed.

S Table 1:

[64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[0, 0, 0, 4, 8, 4, 6, 2, 2, 14, 8, 6, 0, 2, 4, 4]
[0, 0, 0, 2, 10, 10, 12, 6, 0, 2, 4, 6, 0, 4, 6, 2]
[4, 8, 4, 8, 6, 2, 0, 4, 4, 2, 2, 2, 4, 8, 4, 2]
[0, 0, 2, 0, 8, 6, 4, 4, 0, 6, 10, 4, 0, 6, 0, 14]
[2, 0, 4, 8, 2, 4, 10, 2, 2, 0, 6, 4, 4, 4, 6, 6]
[0, 12, 6, 4, 2, 0, 0, 0, 2, 8, 2, 8, 6, 4, 6, 4]
[2, 6, 6, 4, 4, 6, 2, 6, 6, 4, 2, 4, 2, 4, 4, 2]
[0, 0, 0, 4, 6, 0, 6, 4, 0, 10, 16, 8, 0, 2, 0, 8]
[14, 4, 4, 10, 2, 2, 2, 2, 2, 4, 0, 0, 2, 8, 2, 6]
[2, 6, 6, 2, 2, 2, 2, 2, 6, 2, 2, 4, 10, 4, 10, 2]
[6, 2, 2, 0, 6, 4, 4, 8, 10, 2, 0, 8, 2, 2, 6, 2]
[0, 2, 0, 4, 4, 8, 6, 4, 0, 6, 2, 4, 0, 14, 0, 10]
[6, 0, 6, 2, 8, 2, 2, 6, 0, 10, 4, 2, 10, 2, 0, 4]
[0, 6, 10, 8, 8, 2, 0, 6, 8, 2, 4, 4, 0, 4, 2, 0]
[0, 8, 4, 0, 6, 0, 4, 2, 4, 4, 4, 6, 6, 6, 8, 2]
[0, 0, 0, 8, 0, 10, 6, 4, 0, 2, 8, 8, 0, 6, 10, 2]
[6, 6, 4, 6, 2, 4, 0, 0, 6, 2, 10, 2, 4, 0, 8, 4]
[0, 6, 2, 8, 4, 4, 2, 2, 6, 4, 0, 4, 2, 4, 12, 4]
[4, 0, 4, 0, 4, 8, 2, 14, 0, 2, 2, 6, 6, 6, 6, 0]
[0, 6, 6, 4, 4, 4, 2, 2, 8, 2, 2, 2, 10, 0, 0, 12]
[6, 8, 2, 2, 2, 12, 10, 2, 2, 2, 2, 2, 8, 2, 0, 2]
[0, 8, 6, 4, 6, 0, 6, 6, 6, 4, 4, 2, 4, 2, 4, 2]
[6, 4, 8, 4, 4, 4, 4, 2, 6, 2, 4, 4, 4, 4, 0, 4]
[0, 6, 4, 6, 4, 8, 2, 6, 2, 8, 0, 0, 10, 4, 2, 2]
[2, 4, 4, 4, 2, 6, 4, 2, 6, 4, 6, 8, 4, 2, 4, 2]
[0, 6, 8, 2, 2, 6, 4, 8, 8, 2, 2, 6, 2, 4, 2, 2]
[2, 6, 4, 4, 4, 2, 8, 2, 2, 2, 2, 4, 0, 12, 6, 4]
[0, 2, 6, 6, 0, 0, 10, 4, 10, 2, 6, 2, 12, 0, 4, 0]
[0, 6, 2, 2, 0, 4, 6, 8, 4, 6, 2, 0, 6, 0, 4, 14]
[0, 4, 8, 2, 4, 4, 0, 6, 8, 4, 2, 2, 8, 6, 6, 0]
[4, 2, 6, 6, 4, 0, 0, 2, 8, 4, 10, 6, 2, 2, 2, 6]
[0, 0, 2, 2, 12, 2, 8, 10, 0, 0, 0, 2, 0, 12, 10, 4]
[0, 4, 6, 6, 6, 4, 0, 6, 2, 6, 4, 2, 2, 10, 4, 2]
[4, 12, 6, 4, 2, 6, 0, 2, 2, 8, 8, 6, 2, 2, 0, 0]
[8, 2, 2, 2, 4, 4, 2, 4, 4, 8, 2, 2, 8, 4, 2, 6]
[8, 4, 0, 0, 8, 0, 4, 4, 6, 8, 6, 10, 0, 4, 0, 2]
[6, 0, 12, 2, 0, 0, 2, 2, 0, 6, 2, 6, 8, 6, 10, 2]
[2, 4, 4, 4, 2, 4, 6, 2, 2, 0, 4, 2, 2, 2, 10, 14]
[6, 0, 0, 2, 8, 0, 6, 6, 4, 4, 8, 6, 6, 2, 2, 4]
[8, 0, 8, 0, 6, 4, 0, 10, 2, 6, 2, 0, 4, 12, 2, 0]
[0, 2, 4, 10, 10, 0, 2, 4, 0, 10, 2, 2, 0, 8, 6, 4]
[4, 0, 4, 8, 2, 2, 4, 4, 4, 6, 2, 6, 6, 2, 6, 4]
[2, 2, 6, 4, 4, 4, 8, 2, 2, 8, 8, 2, 0, 4, 2, 6]
[10, 6, 8, 6, 0, 0, 2, 4, 4, 0, 4, 4, 0, 6, 4, 6]
[4, 2, 2, 4, 10, 2, 12, 4, 8, 4, 4, 6, 0, 0, 0, 2]
[2, 4, 0, 2, 8, 8, 10, 2, 2, 2, 6, 4, 10, 2, 2, 0]
[10, 4, 6, 8, 0, 8, 4, 0, 0, 4, 0, 2, 2, 6, 2, 8]
[0, 4, 0, 2, 0, 0, 0, 6, 10, 8, 2, 12, 4, 4, 8, 4]
[0, 10, 2, 2, 6, 4, 4, 8, 6, 0, 2, 0, 6, 2, 10, 2]
[8, 2, 6, 0, 2, 2, 2, 2, 4, 6, 8, 0, 6, 4, 4, 8]
[2, 2, 6, 10, 4, 2, 2, 0, 4, 4, 10, 8, 4, 0, 0, 6]
[0, 12, 6, 4, 4, 2, 4, 4, 4, 0, 4, 4, 6, 2, 4, 4]
[0, 12, 2, 6, 2, 8, 0, 6, 10, 0, 0, 8, 2, 4, 4, 0]
[8, 2, 4, 0, 6, 16, 2, 2, 2, 8, 4, 2, 4, 0, 2, 2]
[6, 4, 2, 2, 2, 2, 4, 6, 2, 2, 6, 2, 6, 6, 4, 8]
[2, 8, 8, 10, 4, 0, 2, 10, 4, 0, 4, 2, 6, 2, 2, 0]
[0, 2, 0, 0, 4, 4, 4, 6, 10, 2, 8, 4, 8, 0, 10, 2]
[4, 0, 2, 10, 4, 8, 10, 2, 4, 8, 2, 0, 4, 2, 2, 2]
[16, 2, 4, 2, 0, 2, 2, 4, 4, 4, 4, 2, 8, 2, 2, 6]
[0, 2, 6, 2, 4, 10, 4, 0, 10, 2, 4, 4, 6, 4, 6, 0]
[0, 16, 10, 2, 0, 6, 2, 0, 6, 0, 0, 8, 4, 2, 6, 2]
[4, 4, 0, 10, 0, 0, 6, 0, 4, 2, 4, 6, 4, 4, 2, 14]
[4, 0, 0, 2, 4, 14, 10, 6, 2, 2, 4, 4, 0, 8, 0, 4]

TASK 3:

Current set of possible keys for key part 1:
	[0, 1, 5, 9, 12, 13, 20, 24, 49, 51, 54, 58, 61, 63]
Current set of possible keys for key part 1:
	[49, 51, 61, 63]
Current set of possible keys for key part 1:
	[51, 63]
Final set of possible keys for key part 1:
	[51, 63]

TASK 4:

Current set of possible keys for key part 2:
	[1, 2, 5, 6, 34, 38, 42, 46, 51, 55, 59, 63]
Current set of possible keys for key part 2:
	[1, 5, 51, 55]
Current set of possible keys for key part 2:
	[51, 55]
Final set of possible keys for key part 2:
	[51, 55]
	
*/

